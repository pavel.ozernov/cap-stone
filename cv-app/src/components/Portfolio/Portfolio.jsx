import React, { useEffect, useState } from "react";
import { portfolioData } from "../../utils/portfolioData";
import Isotope from "isotope-layout";
import FilterTabs from "./FilterTabs"; // import the component

const Portfolio = () => {
  const [filterKey, setFilterKey] = useState("*");
  const [isotope, setIsotope] = useState(null);

  // Initialize Isotope after the first render
  useEffect(() => {
    const iso = new Isotope(".filter-container", {
      itemSelector: ".portfolio-item",
      layoutMode: "fitRows",
    });
    setIsotope(iso);
  }, []);

  // Apply Isotope filter whenever filterKey state is updated
  useEffect(() => {
    if (isotope) {
      filterKey === "*"
        ? isotope.arrange({ filter: `*` })
        : isotope.arrange({ filter: `.${filterKey}` });
      console.log("USEEFFECT");
    }
  }, [isotope, filterKey]);

  const handleClick = (key) => {
    setFilterKey(key);
  };

  return (
    <>
      <FilterTabs filterKey={filterKey} handleClick={handleClick} />{" "}
      <ul className="filter-container">
        {portfolioData.map((item) => (
          <li key={item.id} className={`portfolio-item ${item.filter}`}>
            <img src={item.imageSrc} alt={item.title} />
            <div className="portfolio-info">
              <h2>{item.title}</h2>
              <p>{item.description}</p>
              <a href={item.link}>View more</a>
            </div>
          </li>
        ))}
      </ul>
    </>
  );
};

export default Portfolio;
