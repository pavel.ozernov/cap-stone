const Panel = ({ isOpen, isCollapsed, children }) => {
  return (
    <aside
      className={`panel${isCollapsed ? "_collapsed" : ""} ${
        isOpen ? "open" : ""
      } `}
    >
      {children}
    </aside>
  );
};

export default Panel;
