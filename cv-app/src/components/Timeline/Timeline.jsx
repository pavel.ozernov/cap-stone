import timelineData from "../../utils/timelineData";

const Timeline = () => {
  return (
    <ul className="timeline-list">
      {timelineData.map((item, index) => (
        <li key={index}>
          <div className="timeline-date">{item.date}</div>
          <div className="general-event timeline-event">
            <div className="info">
              <h3>{item.title}</h3>
              <p>{item.description}</p>
            </div>
          </div>
        </li>
      ))}
    </ul>
  );
};

export default Timeline;