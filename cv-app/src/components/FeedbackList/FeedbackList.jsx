import feedbackData from "../../utils/feedbackData";
import FeedbackItem from "../FeedbackItem/FeedbackItem";

const FeedbackList = () => {
  return (
    <div data-testid="mountNode">
      <ul className="feedback" style={{ listStyleType: "none", padding: 0 }}>
        {feedbackData.map((item, index) => (
          <FeedbackItem
            key={index}
            content={item.content}
            reporter={item.reporter}
          />
        ))}
      </ul>
    </div>
  );
};

export default FeedbackList;
