import React from "react";
import { Link } from "react-scroll";

function NavigationItem({ text, icon, sectionId }) {
  return (
    <li>
      <Link
        activeClass="active"
        to={sectionId}
        spy={true}
        smooth={true}
        offset={-50}
        duration={500}
      >
        {icon}
        <span>{text}</span>
      </Link>
    </li>
  );
}

export default NavigationItem;
