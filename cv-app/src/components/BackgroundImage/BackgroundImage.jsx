import "./BackgroundImage.css";

export function BackgroundImage({ src, children }) {
  const style = {
    backgroundImage: `url(${src})`,
    backgroundPosition: "center",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    height: "100vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    position: "relative", // Add position relative
  };

  return (
    <div style={style}>
      <div className="overlay"></div> {/* Add the overlay */}
      <div style={{ position: "absolute", zIndex: 2 }}>{children}</div>{" "}
      {/* Position the PhotoBox on top */}
    </div>
  );
}
