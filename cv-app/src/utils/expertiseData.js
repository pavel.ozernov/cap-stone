const expertiseData = [
    {
      company: 'Google',
      date: '2013-2014',
      role: 'Front-end developer / PHP programmer',
      description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
    },
    {
      company: 'Twitter',
      date: '2012',
      role: 'Web developer',
      description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
    },
  ];

  export default expertiseData