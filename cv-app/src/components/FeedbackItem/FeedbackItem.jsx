const FeedbackItem = ({ content, reporter }) => {
  return (
    <li>
      <blockquote>
        <div className="info">
          <h3>{content.title}</h3>
          <p>{content.description}</p>
        </div>
        <div className="feedback-reporter">
          <img
            className="feedback-reporter-photo"
            src={reporter.photo}
            alt="Reporter photo"
          />
          <cite>
            {reporter.name}, <a href={reporter.website}>{reporter.website}</a>
          </cite>
        </div>
      </blockquote>
    </li>
  );
};

export default FeedbackItem;
