const PortfolioItem = ({ imageSrc, title, description, link, filter }) => {
  return (
    <li className={`filter-item ${filter}`}>
      <img src={imageSrc} alt={title} />
      <div className="portfolio-info">
        <h2>{title}</h2>
        <p>{description}</p>
        <a href={link}>View source</a>
      </div>
    </li>
  );
};

export default PortfolioItem;
