import { faPhoneAlt, faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { faTwitter, faFacebook, faSkype } from "@fortawesome/free-brands-svg-icons";


export const contactsData = [
  {
    label: "Phone",
    value: <a href="tel:+500 342 242">500 342 242</a>,
    icon: faPhoneAlt,
  },
  {
    label: "Email",
    value: <a href="mailto:office@kamsolutions.pl">office@kamsolutions.pl</a>,
    icon: faEnvelope,
  },
  {
    label: "Twitter",
    value: <a href="https://twitter.com/wordpress">https://twitter.com/wordpress</a>,
    icon: faTwitter,
  },
  {
    label: "Facebook",
    value: <a href="https://www.facebook.com/facebook">https://www.facebook.com/facebook</a>,
    icon: faFacebook,
  },
  {
    label: "Skype",
    value: <a href="skype:kamsolutions.pl">kamsolutions.pl</a>,
    icon: faSkype,
  },
];
