import React, { useRef } from "react";
import NavigationItem from "../NavigationItem/NavigationItem";
import { navigationItems } from "../../utils/NavigationItems";

function Navigation() {
  const navigationRef = useRef(null);

  return (
    <div ref={navigationRef}>
      <div className="navigation">
        {navigationItems.map(({ text, icon, sectionId }, index) => (
          <NavigationItem
            key={index}
            text={text}
            icon={icon}
            sectionId={sectionId}
          />
        ))}
      </div>
    </div>
  );
}

export default Navigation;
