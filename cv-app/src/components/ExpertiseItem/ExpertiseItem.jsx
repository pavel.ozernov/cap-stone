const ExpertiseItem = ({ company, date, role, description }) => {
    return (
      <li>
        <div className="expertise-list-date">
          <h3>{company}</h3>
          <span className="date">{date}</span>
        </div>
        <div className="expertise-list-info">
          <h3>{role}</h3>
          <p>{description}</p>
        </div>
      </li>
    );
  };
  
  export default ExpertiseItem;