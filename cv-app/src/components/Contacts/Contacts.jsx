import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { contactsData } from "../../utils/contactsData";

const Contacts = () => {
  return (
    <div className="contacts">
      <address className="address">
        {contactsData.map(({ label, value, icon }, index) => (
          <dl key={index}>
            <dt>
              <FontAwesomeIcon icon={icon} />
            </dt>
            <dd>
              <span>{label}</span>
              {value}
            </dd>
          </dl>
        ))}
      </address>
    </div>
  );
};

export default Contacts;
