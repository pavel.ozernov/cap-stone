const PhotoBox = ({ imageSrc, altText, name, title, description }) => {
  return (
    <figure className="photo-box">
      <div className="crop-photo">
        <img src={imageSrc} alt={altText} />
      </div>
      <figcaption>
        <strong>{name}</strong>
        <article>
          <header className="title">{title}</header>
          <div className="description">{description}</div>
        </article>
      </figcaption>
    </figure>
  );
};

export default PhotoBox;
