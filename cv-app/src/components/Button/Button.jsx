import { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Button({ text, icon }) {
  const [screenWidth, setScreenWidth] = useState(window.innerWidth);

  useEffect(() => {
    // Event listener to update screen width state on window resize
    const handleResize = () => {
      setScreenWidth(window.innerWidth);
    };

    // Add event listener on component mount
    window.addEventListener("resize", handleResize);

    // Remove event listener on component unmount
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  // Check if screen width is less than or equal to 599px
  if (screenWidth <= 599) {
    text = ""; // Set an empty string to remove the text
  }

  return (
    <button className="button">
      {icon && <FontAwesomeIcon icon={icon} />}
      {text}
    </button>
  );
}

export default Button;
