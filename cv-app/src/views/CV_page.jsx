import React, { useState } from "react";
import Navigation from "../components/Navigation/Navigation";
import Timeline from "../components/Timeline/Timeline";
import Box from "../components/Box/Box";
import ExpertiseList from "../components/ExpertiseList/ExpertiseList";
import FeedbackList from "../components/FeedbackList/FeedbackList";
import Contacts from "../components/Contacts/Contacts";
import { Element } from "react-scroll";
import Portfolio from "../components/Portfolio/Portfolio";
import Panel from "../components/Panel/Panel";
import PhotoBox from "../components/PhotoBox/PhotoBox";
import userAvatar from "../assets/User_avatar.png";
import ScrollToTopButton from "../components/ScrollToTop/ScrollToTop";

import { Link } from "react-router-dom";
import Button from "../components/Button/Button";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

const CVpage = () => {
  // Code for humburger button starts
  const [isOpen, setIsOpen] = useState(true);
  const togglePanel = () => {
    setIsOpen(!isOpen);
  };
  // Code for humburger button ends

  return (
    <div className="cv-page">
      <Panel isOpen={true} isCollapsed={!isOpen}>
        <div className="panel-content">
          {/* simple text character button */}
          <div className="hamburger-button" onClick={togglePanel}>
            <span>{isOpen ? "X" : "☰"}</span>
          </div>
          {/* simple text character button */}

          <PhotoBox
            imageSrc={userAvatar}
            altText="User photo"
            name="John Doe"
          />
          <Navigation />
          <Link to="/">
            <Button
              className="button"
              text={isOpen ? " Go back" : ""}
              icon={faChevronLeft}
            />
          </Link>
        </div>
      </Panel>

      <div className="right-section">
        <Element name="about">
          <Box title="About me">
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
              commodo ligula eget dolor. Aenean massa. Cum sociis natoque
              penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              Donec quam felis, ultricies nec, pellentesque eu, pretium quis,
              sem. Nulla consequat massa quis enim. Donec pede justo, fringilla
              vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut,
              imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede
              mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum
              semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula,
              porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem
              ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus
              viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean
              imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper
              ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus,
              tellus eget condimentum rhoncus, sem quam semper libero, sit amet
              adipiscing sem neque
            </p>
          </Box>
        </Element>
        <Element name="education">
          <Box title="Education">
            <Timeline />{" "}
            {/* Render the Timeline component or other content components */}
          </Box>
        </Element>
        <Element name="experience">
          <Box title="Experience">
            <ExpertiseList />
          </Box>
        </Element>
        <Element name="portfolio">
          <Box title="Portfolio">
            <Portfolio />
          </Box>
        </Element>
        <Element name="contacts">
          <Box title="Contacts">
            <Contacts />
          </Box>
        </Element>
        <Element name="feedback">
          <Box title="Feedback">
            <FeedbackList />
          </Box>
        </Element>
        <ScrollToTopButton />
      </div>
    </div>
  );
};

export default CVpage;
