import expertiseData from "../../utils/expertiseData";
import ExpertiseItem from "../ExpertiseItem/ExpertiseItem";

const ExpertiseList = () => {
    return (
      <div data-testid="mountNode">
        <ul className="expertise-list">
          {expertiseData.map((item, index) => (
            <ExpertiseItem
              key={index}
              company={item.company}
              date={item.date}
              role={item.role}
              description={item.description}
            />
          ))}
        </ul>
      </div>
    );
  };
  
  export default ExpertiseList;