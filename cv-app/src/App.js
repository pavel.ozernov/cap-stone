import HomePage from './views/Home';
import { Route, Routes } from 'react-router-dom';
import CVpage from './views/CV_page';
// import './App.css';

function App() {
  return (
    
      <Routes>
        <Route exact path="/" element={<HomePage />} />
        <Route exact path="/sidebar" element={<CVpage />} />
      </Routes>
    
  
  );
}

export default App;
