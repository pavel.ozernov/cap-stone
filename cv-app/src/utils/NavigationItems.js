import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faGraduationCap, faBriefcase, faCode, faFolderOpen, faEnvelope, faComments } from '@fortawesome/free-solid-svg-icons';

export const navigationItems = [
  { text: 'About me', icon: <FontAwesomeIcon icon={faUser} />, sectionId: 'about' },
  { text: 'Education', icon: <FontAwesomeIcon icon={faGraduationCap} />, sectionId: 'education' },
  { text: 'Experience', icon: <FontAwesomeIcon icon={faBriefcase} />, sectionId: 'experience' },
  { text: 'Skills', icon: <FontAwesomeIcon icon={faCode} />, sectionId: 'skills' },
  { text: 'Portfolio', icon: <FontAwesomeIcon icon={faFolderOpen} />, sectionId: 'portfolio' },
  { text: 'Contacts', icon: <FontAwesomeIcon icon={faEnvelope} />, sectionId: 'contacts' },
  { text: 'Feedbacks', icon: <FontAwesomeIcon icon={faComments} />, sectionId: 'feedback' },
];
