import React from "react";
import { BackgroundImage } from "../components/BackgroundImage/BackgroundImage";
// import { ProfileCard } from "../components/ProfileCard/ProfileCard";
import backgroundImg from "../assets/Bakcground_home.png";
import PhotoBox from "../components/PhotoBox/PhotoBox";
import userAvatar from "../assets/User_avatar.png";
import { Link } from "react-router-dom";
import Button from "../components/Button/Button";

const HomePage = () => {
  return (
    <BackgroundImage src={backgroundImg}>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          color: "white",
        }}
      >
        <PhotoBox
          imageSrc={userAvatar}
          altText="profile"
          name="John Doe"
          title="Programmer. Creative. Innovator"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque"
        />
        <Link to="/sidebar">
          <div style={{ marginTop: "20px" }}>
            <Button text="Know more" />
          </div>
        </Link>
      </div>
    </BackgroundImage>
  );
};

export default HomePage;
