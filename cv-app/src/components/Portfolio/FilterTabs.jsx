const FilterTabs = ({ filterKey, handleClick }) => {
  return (
    <ul className="tabs">
      <li
        onClick={() => handleClick("*")}
        className={filterKey === "*" ? "active" : ""}
      >
        <span>All</span>
      </li>
      <li
        onClick={() => handleClick("ui")}
        className={filterKey === "ui" ? "active" : ""}
      >
        <span>UI</span>
      </li>
      <li
        onClick={() => handleClick("code")}
        className={filterKey === "code" ? "active" : ""}
      >
        <span>Code</span>
      </li>
    </ul>
  );
};

export default FilterTabs;
