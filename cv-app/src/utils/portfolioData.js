import card1Image from "../assets/card_1.png";
import card2Image from "../assets/card_3.png";

export const portfolioData = [
  {
    id: 1,
    imageSrc: card1Image,
    title: "Item Title UI1",
    description: "Item Description",
    link: "item-link",
    filter: "ui", // Updated filter value to a single string
  },
  {
    id: 2,
    imageSrc: card2Image,
    title: "Item Title Code1",
    description: "Item Description",
    link: "item-link",
    filter: "code", // Updated filter value to a single string
  },
  {
    id: 3,
    imageSrc: card1Image,
    title: "Item Title Code2",
    description: "Item Description",
    link: "item-link",
    filter: "code", // Updated filter value to a single string
  },
  {
    id: 4,
    imageSrc: card2Image,
    title: "Item Title UI2",
    description: "Item Description",
    link: "item-link",
    filter: "ui", // Updated filter value to a single string
  },
];
